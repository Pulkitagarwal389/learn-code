#include <iostream>
using namespace std;
void FootBall(){
            int numberofPass,pastPlayer,currentPlayer;
    		cin>>numberofPass; //Total number of players involved
		if(numberofPass>=1 && numberofPass<=100000){
		cin>>pastPlayer; 	//Past player as an input
		if(pastPlayer>=1 && pastPlayer<=1000000){
    		currentPlayer=pastPlayer;
    		while(numberofPass--)
	            	{
        	        	char typeofPass;
        		        cin>>typeofPass; //selecting the type of pass
        	           	if(typeofPass == 'P')  //If its a pass , change both past and current player
			            {
            			    int playerID;
            		    	cin>>playerID;
            			    pastPlayer = currentPlayer;
            			    currentPlayer = playerID;
        		        }
        		        else                   //If its not a pass , swap both values
		            	{
            		    	int temp;
            		    	temp = pastPlayer;
            		    	pastPlayer = currentPlayer;
            		    	currentPlayer = temp;
        		        }
   		             }
    		cout<<"Player "<<currentPlayer<<endl;
            		}
		}
	}	
int main() 
{
	int testCases;
    	cin>>testCases;
	if(testCases>=1 && testCases <=100){
    	while(testCases--) //loop for testcaes 
	    {
   	 	FootBall(); //Function performing logic for each loop
    	    }
	}
    	return 0;
}
