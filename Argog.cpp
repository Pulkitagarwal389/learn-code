#include <iostream>
#include <queue>
const int rangeMin = 1;
const int rangeMax = 316;
using namespace std;
int maxSpiderPosition,spiderPowerValue,spiderPowerIndex,spiderQueueSize,maximumSpiderPower;
queue <int> argogSpidersPower,argogSpidersPowerTemp,argogSpidersPosition,argogSpidersPositionTemp;
 
bool IsConstrait(int numberOfSpiderSelected)
{
    if(numberOfSpiderSelected >= rangeMin && numberOfSpiderSelected <= rangeMax)
    {
        return true;  
    } 
    else 
    {
        return false;
    }
}

void DecideQueueSize(int numberOfSpiderSelected)
{
    if((argogSpidersPower.size())>numberOfSpiderSelected)
    {
        spiderQueueSize=numberOfSpiderSelected;  
    }  	
    else
    {
        spiderQueueSize = argogSpidersPower.size();  
    } 
}

void LoadSpiders(int numberOfSpider , int numberOfSpiderSelected)
{
     if(numberOfSpider>=numberOfSpiderSelected && numberOfSpider<=(numberOfSpiderSelected)*(numberOfSpiderSelected))
     {
        for(int powerInputCounter=0;powerInputCounter<numberOfSpider;powerInputCounter++)
        {
           cin>>spiderPowerValue;
	   if(spiderPowerValue>=1 && spiderPowerValue<=numberOfSpiderSelected)
           {
                argogSpidersPower.push(spiderPowerValue);
            	argogSpidersPosition.push(powerInputCounter+1);
     	   }
        }
     }
} 
 
int FindMaxSpiderPower(int spiderQueueSize)
{
    int maximumSpiderPower = 0;
    for(int spiderCounter=1;spiderCounter<=spiderQueueSize;spiderCounter++)
    {
            spiderPowerValue = argogSpidersPower.front();
            spiderPowerIndex = argogSpidersPosition.front();
            if(spiderPowerValue>maximumSpiderPower)
            {
                maximumSpiderPower=spiderPowerValue;
            }
            argogSpidersPowerTemp.push(spiderPowerValue);
            argogSpidersPositionTemp.push(spiderPowerIndex);
            argogSpidersPower.pop();
            argogSpidersPosition.pop();
    }
    return maximumSpiderPower;
}
 
void DecreaseSpiderPower(int spiderQueueSize, int maximumSpiderPower)
{
    int flag=0;
    for(int spiderCounter=1;spiderCounter<=spiderQueueSize;spiderCounter++)
    {
            spiderPowerValue = argogSpidersPowerTemp.front();
            spiderPowerIndex = argogSpidersPositionTemp.front();
            if(spiderPowerValue!=maximumSpiderPower || flag==1)
	    {
                spiderPowerValue--;
                if(spiderPowerValue<0)
                {
                    spiderPowerValue=0;
                }
                argogSpidersPower.push(spiderPowerValue);
                argogSpidersPosition.push(spiderPowerIndex);
            }
            //Assigning Index of maximum value Spider
            else if (spiderPowerValue==maximumSpiderPower)
            { 
                flag=1; maxSpiderPosition = spiderPowerIndex;
            }				
            argogSpidersPowerTemp.pop();
            argogSpidersPositionTemp.pop();
    }
}
 
int main()
{
   int numberOfSpider,numberOfSpiderSelected;
   cin>>numberOfSpider>>numberOfSpiderSelected;
   LoadSpiders(numberOfSpider,numberOfSpiderSelected);			
   if(IsConstrait(numberOfSpiderSelected))
   {
        for(int questionCounter=1;questionCounter<=numberOfSpiderSelected;questionCounter++)
        {           
            //Number of spiders change after every iteration
            DecideQueueSize(numberOfSpiderSelected);
            maximumSpiderPower = FindMaxSpiderPower(spiderQueueSize);
            DecreaseSpiderPower(spiderQueueSize,maximumSpiderPower);  
            cout<<maxSpiderPosition<<" ";
        }
   }
   return 0;
}