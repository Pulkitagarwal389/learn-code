#include <iostream>
using namespace std;
 
int main()
{
  long long int testCases;
   cin>>testCases;
   if(testCases>=1 && testCases<= 100){        //Test cases loop with contraints
   while(testCases--){
     long long  int currentNumber,previousNumber,flag=0,count=0;
       cin>>currentNumber;
       if(currentNumber>=1 && currentNumber<=100000){
       cin>>previousNumber;
       if(previousNumber>=1 && previousNumber<=1000000){
          long long int a[currentNumber];
           char b[currentNumber];		//Below 2 for loop to get the last occurance for pass in flag to do optimization
        for(int i=0;i<currentNumber;i++){
            cin>>b[i];
            if(b[i]=='P'){		//If its a pass
                int temp;
                cin>>temp;
                a[i]=temp;
            }
            else a[i]=0;
        }
        for(int i=currentNumber-1;i>=0;i--){
            if(b[i]=='P') count++;
            if(b[i]=='B') count=0;
            if(count==2){
                flag=i;		//Index after which calculation really matter
                break;
            }
        }
        int lastOccurance1,lastOccurance2,temp;
        lastOccurance1 = a[flag];
        lastOccurance2 = lastOccurance1;
        for(int i=flag+1;i<currentNumber;i++){		//Calculating from the index where it matter only
            if(b[i]=='P'){
                lastOccurance1 = lastOccurance2;
                lastOccurance2 = a[i];
            }
            else{
                temp = lastOccurance1;
                lastOccurance1 = lastOccurance2;
                lastOccurance2 = temp;
            }
         }
      
   cout<<"Player "<<lastOccurance2<<endl;		//Optimized output logic
            }
        }
     }
   }
    return 0;
}
