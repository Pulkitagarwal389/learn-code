#include<bits/stdc++.h>
using namespace std;
int totalFriends,friendsDecidedToDelete,popularity;
list<int> friendsPopularityArray;
list<int> :: iterator friendIterator;

void FbFriendsInput()
{
     for(int count=0;count<totalFriends;++count)
      {
          scanf("%d",&popularity);
          friendsPopularityArray.push_back(popularity);
      }
}

void FbFriendShow()
{
    for(friendIterator=friendsPopularityArray.begin();friendIterator!=friendsPopularityArray.end();++friendIterator)
          printf("%d ",*friendIterator);
          printf("\n");
}

void FbFriendRemove()
{
    for(friendIterator=friendsPopularityArray.begin();friendIterator!=friendsPopularityArray.end();)
      {
          int currentFriend=*(friendsPopularityArray.begin());
          if(friendsDecidedToDelete==0)
              break;
          int currentFriendPopularity=*friendIterator;
          friendIterator++;
          int nextFriendPopularity=*friendIterator;
          if(currentFriendPopularity>=nextFriendPopularity)
              continue;
          else
          {
              --friendIterator;
              friendsPopularityArray.erase(friendIterator);
              int nextFriend=*friendIterator;
              if(currentFriend!=nextFriend)
                  --friendIterator;
              else
                  friendIterator=friendsPopularityArray.begin();
                  friendsDecidedToDelete--;
          }
      }
      if(friendsDecidedToDelete!=0)
      {
          for(int count=0;count<friendsDecidedToDelete;++count)
              friendsPopularityArray.pop_back();
      }
}

int main()
{
  int testcases;
  scanf("%d",&testcases);
  while(testcases--)
  {
      scanf("%d %d",&totalFriends,&friendsDecidedToDelete);
      FbFriendsInput();
      FbFriendRemove();
      FbFriendShow();
      friendsPopularityArray.clear();
  }
  return 0;
}